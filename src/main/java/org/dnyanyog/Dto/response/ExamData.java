package org.dnyanyog.Dto.response;

import java.time.LocalDate;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
@Component
public class ExamData {
	
    private long id;
	private String className;
	private String subject;
	private LocalDate examDate;
	private String examTime;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public LocalDate getExamDate() {
		return examDate;
	}
	public void setExamDate(LocalDate examDate) {
		this.examDate = examDate;
	}
	public String getExamTime() {
		return examTime;
	}
	public void setExamTime(String examTime) {
		this.examTime = examTime;
	}
	
	
	
	
}
