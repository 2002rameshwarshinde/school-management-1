package org.dnyanyog.Dto.response;

import org.dnyanyog.entity.LeaveStatus;
import org.springframework.stereotype.Service;
@Service
public class LeaveStatusDto {

	private LeaveStatus status;

	public LeaveStatus getStatus() {
		return status;
	}

	public void setStatus(LeaveStatus  status) {
		this.status = status;
	}
}
