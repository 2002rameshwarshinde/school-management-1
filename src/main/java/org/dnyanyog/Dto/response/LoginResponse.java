package org.dnyanyog.Dto.response;

import org.springframework.stereotype.Service;

@Service
public class LoginResponse {

	private String status;
	private String messgae;
	private String Data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessgae() {
		return messgae;
	}
	public void setMessgae(String messgae) {
		this.messgae = messgae;
	}
	public String getData() {
		return Data;
	}
	public void setData(String data) {
		Data = data;
	}
	
}
