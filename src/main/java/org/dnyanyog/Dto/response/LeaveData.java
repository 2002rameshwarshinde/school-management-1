package org.dnyanyog.Dto.response;

import java.time.LocalDate;

import org.dnyanyog.entity.LeaveStatus;
import org.springframework.stereotype.Service;

@Service
public class LeaveData {
 
	private Long id;
	
	private Long grnNo;
	
	private String studentName;
	
	private String reason;
	
	private LocalDate startDate;
	
	private LocalDate endDate;
	
	private LeaveStatus status;
	
	private String response;

	

	

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getGrnNo() {
		return grnNo;
	}

	public void setGrnNo(long grnNo) {
		this.grnNo = grnNo;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	


	public LeaveStatus getStatus() {
		return status;
	}

	public void setStatus(LeaveStatus status) {
		this.status = status;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}


	
	 
}
