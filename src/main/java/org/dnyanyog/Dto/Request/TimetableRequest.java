package org.dnyanyog.Dto.Request;

import org.springframework.stereotype.Service;

@Service
public class TimetableRequest {
 private String weekday;
 private String timeslot;
 private String activity;
 private String Classname;
public String getWeekday() {
	return weekday;
}
public void setWeekday(String weekday) {
	this.weekday = weekday;
}
public String getTimeslot() {
	return timeslot;
}
public void setTimeslot(String timeslot) {
	this.timeslot = timeslot;
}
public String getClassname() {
	return Classname;
}
public void setClassname(String classname) {
	Classname = classname;
}
public String getActivity() {
	return activity;
}
public void setActivity(String activity) {
	this.activity = activity;
}
 
}
