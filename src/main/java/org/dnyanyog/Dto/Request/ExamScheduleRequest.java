package org.dnyanyog.Dto.Request;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

@Component
public class ExamScheduleRequest {

	private String className;
	private String subject;
	private LocalDate examDate;
	private String examTime;
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getExamTime() {
		return examTime;
	}
	public void setExamTime(String examTime) {
		this.examTime = examTime;
	}
	public LocalDate getExamDate() {
		return examDate;
	}
	public void setExamDate(LocalDate examDate) {
		this.examDate = examDate;
	}
	
	
	
}
