package org.dnyanyog.Dto.Request;

import org.springframework.stereotype.Service;

@Service
public class FeesRequest {
 private long grnNo;
 private String studentName;
 private String appliedClass;
 private String totalFees;
 private String feesPaid;
 private String remainingFees;
 private String acadmicYear;


public long getGrnNo() {
	return grnNo;
}

public void setGrnNo(long grnNo) {
	this.grnNo = grnNo;
}

public String getStudentName() {
	return studentName;
}
public void setStudentName(String studentName) {
	this.studentName = studentName;
}
public String getAppliedClass() {
	return appliedClass;
}
public void setAppliedClass(String appliedClass) {
	this.appliedClass = appliedClass;
}
public String getTotalFees() {
	return totalFees;
}
public void setTotalFees(String totalFees) {
	this.totalFees = totalFees;
}
public String getFeesPaid() {
	return feesPaid;
}
public void setFeesPaid(String feesPaid) {
	this.feesPaid = feesPaid;
}
public String getRemainingFees() {
	return remainingFees;
}
public void setRemainingFees(String remainingFees) {
	this.remainingFees = remainingFees;
}
public String getAcadmicYear() {
	return acadmicYear;
}
public void setAcadmicYear(String acadmicYear) {
	this.acadmicYear = acadmicYear;
}
 
 
}
