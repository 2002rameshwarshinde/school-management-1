package org.dnyanyog.Dto.Request;

import java.time.LocalDate;

import org.springframework.stereotype.Service;
@Service
public class LeaveApplicationRequest {
	    private Long grnNo;
	    private String studentName;
	    private LocalDate startDate;
	    private LocalDate endDate;
	    private String reason;
		public String getStudentName() {
			return studentName;
		}
		public void setStudentName(String studentName) {
			this.studentName = studentName;
		}
		public LocalDate getStartDate() {
			return startDate;
		}
		public void setStartDate(LocalDate startDate) {
			this.startDate = startDate;
		}
		public LocalDate getEndDate() {
			return endDate;
		}
		public void setEndDate(LocalDate endDate) {
			this.endDate = endDate;
		}
		public String getReason() {
			return reason;
		}
		public void setReason(String reason) {
			this.reason = reason;
		}
		public long getGrnNo() {
			return grnNo;
		}
		public void setGrnNo(long grnNo) {
			this.grnNo = grnNo;
		}
		
	    
	    

	   
	}


