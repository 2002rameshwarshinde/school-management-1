package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

@Entity
@Table
@Component
public class FeesInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private long grnNo;

    @Column
    private String studentName;

    @Column
    private String appliedClass;

    @Column
    private String totalFees;

    @Column
    private String feesPaid;

    @Column
    private String remainingFees;

    @Column
    private String acadmicYear;

   
  
    

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getGrnNo() {
		return grnNo;
	}

	public void setGrnNo(long grnNo) {
		this.grnNo = grnNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getAppliedClass() {
		return appliedClass;
	}

	public void setAppliedClass(String appliedClass) {
		this.appliedClass = appliedClass;
	}

	public String getTotalFees() {
		return totalFees;
	}

	public void setTotalFees(String totalFees) {
		this.totalFees = totalFees;
	}

	public String getFeesPaid() {
		return feesPaid;
	}

	public void setFeesPaid(String feesPaid) {
		this.feesPaid = feesPaid;
	}

	
	public String getRemainingFees() {
		return remainingFees;
	}


	public void setRemainingFees(String remainingFees) {
		this.remainingFees = remainingFees;
	}


	public String getAcadmicYear() {
		return acadmicYear;
	}

	public void setAcadmicYear(String acadmicYear) {
		this.acadmicYear = acadmicYear;
	}

	  public FeesInfo() {
	    }

	    
	    public FeesInfo(long id, long grnNo, String studentName, String appliedClass, String totalFees, String feesPaid, String acadmicYear, String remainingFees) {
	        this.id = id;
	        this.grnNo = grnNo;
	        this.studentName = studentName;
	        this.appliedClass = appliedClass;
	        this.totalFees = totalFees;
	        this.feesPaid = feesPaid;
	        this.acadmicYear = acadmicYear;
	        this.remainingFees = remainingFees;
	    }


}
