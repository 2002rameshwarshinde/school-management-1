package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Table
@Entity 
@Component
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private long Id;
 
 
 @Column
 private String StudentName;
 
 @Column
 private String fatherName;
 
 @Column
 private String MotherName;
 
 @Column
 private String Lastname;
 
 @Column(unique = true)
 private String parentMobileNo;
 
 @Column
 private String Address;
 
 
@Column
 private String BirthDate;
 
 @Column
 private String Gender;
 
 @Column 
 private String AppliedClass;
 
 @Column
 private String PreviousSchoolName;
 
 @Column
 private String studentMob;
 
 @Column
 private String email;


 public long getId() {
	return Id;
}

public void setId(long id) {
	Id = id;
}

public String getStudentName() {
	return StudentName;
}

public void setStudentName(String studentName) {
	StudentName = studentName;
}

public String getFatherName() {
	return fatherName;
}

public void setFatherName(String fatherName) {
	this.fatherName = fatherName;
}

public String getMotherName() {
	return MotherName;
}

public void setMotherName(String motherName) {
	MotherName = motherName;
}

public String getLastname() {
	return Lastname;
}

public void setLastname(String lastname) {
	Lastname = lastname;
}



public String getBirthDate() {
	return BirthDate;
}

public void setBirthDate(String birthDate) {
	BirthDate = birthDate;
}

public String getGender() {
	return Gender;
}

public void setGender(String gender) {
	Gender = gender;
}

public String getAppliedClass() {
	return AppliedClass;
}

public void setAppliedClass(String appliedClass) {
	AppliedClass = appliedClass;
}

public String getPreviousSchoolName() {
	return PreviousSchoolName;
}

public void setPreviousSchoolName(String previousSchoolName) {
	PreviousSchoolName = previousSchoolName;
}

public String getStudentMob() {
	return studentMob;
}

public void setStudentMob(String studentMob) {
	this.studentMob = studentMob;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}


public String getParentMobileNo() {
	return parentMobileNo;
}

public void setParentMobileNo(String parentMobileNo) {
	this.parentMobileNo = parentMobileNo;
}

public String getAddress() {
	return Address;
}

public void setAddress(String address) {
	Address = address;
}
 
}
