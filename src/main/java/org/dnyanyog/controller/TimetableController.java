package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.entity.Timetable;
import org.dnyanyog.service.Timetableservice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

@RestController

public class TimetableController {

    private Timetableservice timetableService;

    public TimetableController(Timetableservice timetableEntryService) {
        this.timetableService = timetableEntryService;
    }

    @GetMapping
    public List<Timetable> getAllTimetableEntries() {
        return timetableService.getAllTimetableEntries();
    }

    @GetMapping(path="Timetable/api/{className}")
    public List<Timetable> getTimetableEntriesByClass(@PathVariable String className) {
        return timetableService.getTimetableEntriesByClass(className);
    }

    @PostMapping(path="Timetable/api/add")
    public Timetable createTimetable(@RequestBody Timetable timetable) {
     
        return timetableService.createTimetable(timetable);
    }

}

