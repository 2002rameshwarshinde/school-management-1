package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.Dto.Request.FeesRequest;
import org.dnyanyog.Dto.response.FeesResponse;
import org.dnyanyog.entity.FeesInfo;
import org.dnyanyog.service.FeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeesController {

	@Autowired
	FeesService feesService;

	@PostMapping(path = "Fees/api/add")
	public ResponseEntity<FeesResponse> saveData(@RequestBody FeesRequest feesRequest) {
		return feesService.saveData(feesRequest);
	}

	@GetMapping(path = "/fees/api/{grnNo}")
	public List<FeesInfo> getFeesInfo(@PathVariable long grnNO) {
		return feesService.getFeesInfo(grnNO);

	}

	@DeleteMapping("/Fees/api/delete/{grnNo}")
	public ResponseEntity<?> deleteFeesInfoByGrnNo(@PathVariable("grnNo") long grnNo) {
		boolean deleted = feesService.deleteFeesInfoByGrnNo(grnNo);
		if (deleted) {
			return ResponseEntity.ok("Fees With GrnNO"+grnNo+"Deleted Succcessfully");
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@PutMapping("/Fees/api/update/{id}")
	public ResponseEntity<FeesInfo> updateFeesInfo(@PathVariable Long id, @RequestBody FeesInfo updatedFeesInfo) {
		FeesInfo updatedFeesInfoRecord = feesService.updateFeesInfo(id, updatedFeesInfo);

		if (updatedFeesInfoRecord != null) {
			return ResponseEntity.ok(updatedFeesInfoRecord);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
