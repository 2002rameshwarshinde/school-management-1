package org.dnyanyog.controller;

import org.dnyanyog.Dto.Request.RegistrationRequest;
import org.dnyanyog.Dto.response.RegistrationResponse;
import org.dnyanyog.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {

	@Autowired
	RegistrationService service;
	
	@PostMapping(path="registration/api/v1/user")
	public ResponseEntity<RegistrationResponse> addUser(@RequestBody RegistrationRequest request) {
		return service.saveData(request);
	}
}
