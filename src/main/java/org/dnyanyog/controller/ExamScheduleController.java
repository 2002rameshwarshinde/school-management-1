package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.Dto.Request.ExamScheduleRequest;
import org.dnyanyog.Dto.response.ExamResponse;
import org.dnyanyog.entity.ExamSchedule;

import org.dnyanyog.service.ExamScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@RestController 
public class ExamScheduleController {

	@Autowired
	ExamScheduleService examScheduleService;

	@PostMapping(path = "Exam/api/Add")
	public ResponseEntity<ExamResponse> addEntry(@RequestBody ExamScheduleRequest request) {

		return examScheduleService.saveData(request);
	}

	@GetMapping(path = "/Exam/api/Find/{className}")
	public List<ExamSchedule> getFeesInfo(@PathVariable String className) {
		return examScheduleService.getExamScheduleByClass(className);

	}
	
	 @PutMapping(path="/Exam/api/update/{id}")
	    public ResponseEntity<ExamSchedule> updateExamSchedule(  @PathVariable Long id, @RequestBody ExamSchedule updatedExamSchedule) 
	 {
	        ExamSchedule updatedSchedule = examScheduleService.updateExamSchedule(id, updatedExamSchedule);
	        if (updatedSchedule != null) {
	            return ResponseEntity.ok(updatedSchedule);
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    }
	 
	 @DeleteMapping(path="/Exam/api/delete/{className}")
	    public ResponseEntity<String> deleteExamSchedulesByClassName(@PathVariable String className) {
	        int deletedCount = examScheduleService.deleteExamSchedulesByClassName(className);
	        return ResponseEntity.ok("Deleted " + deletedCount + " exam schedule(s) for class " + className);
	    }
}
