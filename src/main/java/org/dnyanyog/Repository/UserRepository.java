package org.dnyanyog.Repository;

import org.dnyanyog.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	
	User findBygrnNo(boolean b);

	User findBymobileNo(String mobileNo);

}
