package org.dnyanyog.Repository;

import java.util.List;

import org.dnyanyog.entity.Timetable;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
@Repository
public interface TimetableRepository extends JpaRepository<Timetable,Long>{

	  List<Timetable> findByClassName(String className);
	  
}
