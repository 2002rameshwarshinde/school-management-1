package org.dnyanyog.Repository;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.entity.FeesInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface FeesRepository extends JpaRepository<FeesInfo, Long> {

    List<FeesInfo> findByGrnNo(long grnNo);

    List<FeesInfo> findAllByStudentName(String studentName);

    List<FeesInfo> findAllByAppliedClass(String appliedClass);
    
  //  List<FeesInfo> findById(long id);
    
    boolean existsByGrnNo(long grnNo);

    void deleteByGrnNo(long grnNo);
    
    Optional<FeesInfo>findById(long id);
}

	
	

 