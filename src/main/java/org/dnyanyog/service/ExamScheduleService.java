package org.dnyanyog.service;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.Dto.Request.ExamScheduleRequest;
import org.dnyanyog.Dto.response.ExamData;
import org.dnyanyog.Dto.response.ExamResponse;
import org.dnyanyog.Repository.ExamScheduleRepository;
import org.dnyanyog.entity.ExamSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class ExamScheduleService {

	
	@Autowired
	ExamScheduleRepository examSchedulerepository;
	
	@Autowired
	ExamSchedule examSchedule;
	
	@Autowired
	ExamResponse response;
	
	@Autowired
	ExamData examData;
	
	public ResponseEntity<ExamResponse>saveData(ExamScheduleRequest examRequest){
		
		response=new ExamResponse();
		response.setData(new ExamData() );
		
		examSchedule=new ExamSchedule();
		
		examSchedule.setClassName(examRequest.getClassName());
		examSchedule.setSubject(examRequest.getSubject());
		examSchedule.setExamDate(examRequest.getExamDate());
		examSchedule.setExamTime(examRequest.getExamTime());

		examSchedule=examSchedulerepository.save(examSchedule);
		
		response.setStatus("Success");
		response.setMessage("Exam Schedule Added SuccessFully");
		response.getData().setId(examSchedule.getId());
		response.getData().setClassName(examSchedule.getClassName());
		response.getData().setSubject(examSchedule.getSubject());
		response.getData().setExamDate(examSchedule.getExamDate());
		response.getData().setExamTime(examSchedule.getExamTime());
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
	public List<ExamSchedule>getExamScheduleByClass(String className){
		
		return examSchedulerepository.findByClassName(className);
	}
	
	 public ExamSchedule updateExamSchedule(Long id, ExamSchedule updatedExamSchedule) {
	        Optional<ExamSchedule> existingSchedule = examSchedulerepository.findById(id);
	        if (existingSchedule.isPresent()) {
	            ExamSchedule scheduleToUpdate = existingSchedule.get();
	            scheduleToUpdate.setClassName(updatedExamSchedule.getClassName());
	            scheduleToUpdate.setSubject(updatedExamSchedule.getSubject());
	            scheduleToUpdate.setExamDate(updatedExamSchedule.getExamDate());
	            scheduleToUpdate.setExamTime(updatedExamSchedule.getExamTime());

	           
	            return examSchedulerepository.save(scheduleToUpdate);
	        }
	        return null; 
	    }
	 
	 public int deleteExamSchedulesByClassName(String className) {
	        List<ExamSchedule> examSchedules = examSchedulerepository.findByClassName(className);
	        examSchedulerepository.deleteAll(examSchedules);
	        return examSchedules.size();
	    }
}
