package org.dnyanyog.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.dnyanyog.Dto.Request.FeesRequest;
import org.dnyanyog.Dto.response.FeesData;
import org.dnyanyog.Dto.response.FeesResponse;
import org.dnyanyog.Repository.FeesRepository;
import org.dnyanyog.entity.FeesInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
public class FeesService {

	@Autowired
	FeesRepository feesRepository;

	@Autowired
	FeesResponse feesResponse;
	@Autowired
	FeesInfo feesinfo;

	@Autowired
	FeesData feesData;

	public ResponseEntity<FeesResponse> saveData(FeesRequest feesRequest) {

		feesResponse = new FeesResponse();
		feesResponse.setData(new FeesData());

		feesinfo = new FeesInfo();
		feesinfo.setGrnNo(feesRequest.getGrnNo());
		feesinfo.setStudentName(feesRequest.getStudentName());
		feesinfo.setAppliedClass(feesRequest.getAppliedClass());
		feesinfo.setTotalFees(feesRequest.getTotalFees());
		feesinfo.setFeesPaid(feesRequest.getFeesPaid());
		feesinfo.setRemainingFees(feesRequest.getRemainingFees());
		feesinfo.setAcadmicYear(feesRequest.getAcadmicYear());

		feesinfo = feesRepository.save(feesinfo);

		feesResponse.setStatus("Success");
		feesResponse.setMessage("Fees Added Successfully");
		feesResponse.getData().setId(feesinfo.getId());
		feesResponse.getData().setGrnNo(feesinfo.getGrnNo());
		feesResponse.getData().setStudentName(feesinfo.getStudentName());
		feesResponse.getData().setAppliedClass(feesinfo.getAppliedClass());
		feesResponse.getData().setTotalFees(feesinfo.getTotalFees());
		feesResponse.getData().setFeesPaid(feesinfo.getFeesPaid());
		feesResponse.getData().setRemainingFees(feesinfo.getRemainingFees());
		feesResponse.getData().setAcadmicYear(feesinfo.getAcadmicYear());

		return ResponseEntity.status(HttpStatus.CREATED).body(feesResponse);

	}

	public List<FeesInfo> getFeesInfo(long grnNo) {

		return feesRepository.findByGrnNo(grnNo);

	}

	@Transactional
	public boolean deleteFeesInfoByGrnNo(long grnNo) {
		if (feesRepository.existsByGrnNo(grnNo)) {
			feesRepository.deleteByGrnNo(grnNo);
			return true;
		} else {
			return false;
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public FeesInfo updateFeesInfo(long id, FeesInfo updateFeesInfo) {
		Optional<FeesInfo> existingFeesInfo = feesRepository.findById(id);

		if (existingFeesInfo.isPresent()) {
			FeesInfo feesInfoToUpdate = existingFeesInfo.get();
			
			 feesInfoToUpdate.setGrnNo(updateFeesInfo.getGrnNo());
	            feesInfoToUpdate.setStudentName(updateFeesInfo.getStudentName());
	            feesInfoToUpdate.setAppliedClass(updateFeesInfo.getAppliedClass());
	            feesInfoToUpdate.setTotalFees(updateFeesInfo.getTotalFees());
	            feesInfoToUpdate.setFeesPaid(updateFeesInfo.getFeesPaid());
	            feesInfoToUpdate.setRemainingFees(updateFeesInfo.getRemainingFees());
	            feesInfoToUpdate.setAcadmicYear(updateFeesInfo.getAcadmicYear());
			return feesRepository.save(feesInfoToUpdate);
		}

		return null;
	}

}
