package org.dnyanyog.service;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.Dto.Request.LeaveApplicationRequest;
import org.dnyanyog.Dto.response.LeaveApplicationResponse;
import org.dnyanyog.Dto.response.LeaveData;
import org.dnyanyog.Repository.LeaveRepository;
import org.dnyanyog.entity.LeaveApplication;
import org.dnyanyog.entity.LeaveStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class LeaveService {

	@Autowired
	private LeaveRepository leaveRequestRepository;

	@Autowired
	LeaveApplication leaveApplication;

	@Autowired
	LeaveApplicationResponse response;

	@Autowired
	LeaveData leaveData;

	public ResponseEntity<LeaveApplicationResponse> applyforLeave(LeaveApplicationRequest leaveApplicationRequest) {

		response = new LeaveApplicationResponse();
		response.setData(new LeaveData());

		leaveApplication = new LeaveApplication();

		leaveApplication.setGrnNO(leaveApplicationRequest.getGrnNo());
		leaveApplication.setStudentName(leaveApplicationRequest.getStudentName());
		leaveApplication.setStartDate(leaveApplicationRequest.getStartDate());
		leaveApplication.setEndDate(leaveApplicationRequest.getEndDate());
		leaveApplication.setReason(leaveApplicationRequest.getReason());
		leaveApplication.setStatus(LeaveStatus.PENDING);
 
		leaveApplication=leaveRequestRepository.save(leaveApplication);
		
		response.setStatus("Success");
		response.setMessage("Leave Applied Successfully");
		response.getData().setId(leaveApplication.getId());
		response.getData().setGrnNo(leaveApplication.getGrnNO());
		response.getData().setStudentName(leaveApplication.getStudentName());
		response.getData().setStartDate(leaveApplication.getStartDate());
		response.getData().setEndDate(leaveApplication.getEndDate());
		response.getData().setReason(leaveApplication.getReason());
		response.getData().setStatus(leaveApplication.getStatus());
		return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}

	public List<LeaveApplication> getLeaveRequestsByStatus(String status) {
		LeaveStatus leaveStatus = LeaveStatus.valueOf(status.toUpperCase());

		return leaveRequestRepository.findByStatus(leaveStatus);
	}

	public boolean approveLeaveRequest(Long requestId) {
		Optional<LeaveApplication> optionalLeaveRequest = leaveRequestRepository.findById(requestId);

		if (optionalLeaveRequest.isPresent()) {
			LeaveApplication leaveRequest = optionalLeaveRequest.get();
			leaveRequest.setStatus(LeaveStatus.APPROVED);
			leaveRequestRepository.save(leaveRequest);
			return true;
		} else {
			return false;
		}
	}

	public boolean rejectLeaveRequest(Long requestId) {
		Optional<LeaveApplication> optionalLeaveRequest = leaveRequestRepository.findById(requestId);

		if (optionalLeaveRequest.isPresent()) {
			LeaveApplication leaveRequest = optionalLeaveRequest.get();
			leaveRequest.setStatus(LeaveStatus.REJECTED);
			leaveRequestRepository.save(leaveRequest);
			return true;
		} else {
			return false;
		}
	}

	public LeaveApplication updateLeave(Long id, LeaveApplication updatedLeave) {
		Optional<LeaveApplication> existingLeave = leaveRequestRepository.findById(id);

		if (existingLeave.isPresent()) {
			LeaveApplication leaveToUpdate = existingLeave.get();

			if (updatedLeave.getGrnNO() != null) {
				leaveToUpdate.setGrnNO(updatedLeave.getGrnNO());
			}
			if (updatedLeave.getStudentName() != null) {
				leaveToUpdate.setStudentName(updatedLeave.getStudentName());
			}
			if (updatedLeave.getStartDate() != null) {
				leaveToUpdate.setStartDate(updatedLeave.getStartDate());
			}
			if (updatedLeave.getEndDate() != null) {
				leaveToUpdate.setEndDate(updatedLeave.getEndDate());
			}
			if (updatedLeave.getReason() != null) {
				leaveToUpdate.setReason(updatedLeave.getReason());
			}
			if (updatedLeave.getStatus() != null) {
				leaveToUpdate.setStatus(updatedLeave.getStatus());
			}
			if (updatedLeave.getResponse() != null) {
				leaveToUpdate.setResponse(updatedLeave.getResponse());
			}

			return leaveRequestRepository.save(leaveToUpdate);
		}

		return null;
	}

	
	
	
	
	
	
	
	public boolean deleteLeaveById(Long id) {
		Optional<LeaveApplication> existingLeave = leaveRequestRepository.findById(id);

		if (existingLeave.isPresent()) {
			leaveRequestRepository.deleteById(id);
			return true;
		}

		return false;
	}

	
	
	
	
	public List<LeaveApplication> getLeaveRequestsByGrnNo(Long grnNo) {
		return leaveRequestRepository.findByGrnNO(grnNo);
	}
}
