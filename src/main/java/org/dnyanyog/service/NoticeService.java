package org.dnyanyog.service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.dnyanyog.Repository.NoticeRepository;
import org.dnyanyog.entity.Notice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoticeService {
	@Autowired
	NoticeRepository noticeRepository;

	@Autowired
	public NoticeService(NoticeRepository noticeRepository) {
		this.noticeRepository = noticeRepository;
	}

	public List<Notice> getNoticeById() {
		return noticeRepository.findAllById(null);
	}

	public Notice createNotice(Notice noticeEntry) {
		return noticeRepository.save(noticeEntry);
	}

	public List<Notice> getNoticesByDate(LocalDate noticeDate) {
		return noticeRepository.findByNoticeDate(noticeDate);
	}
	
	
	 public boolean deleteNoticeById(long id) {
		   Optional<Notice> noticeOptional = noticeRepository.findById(id);
		    if (noticeOptional.isPresent()) {
		        noticeRepository.deleteById(id);
		        return true;
		    } else {
		        return false;
		    }
	 }
	 public Notice updateNotice(long id, Notice updatedNotice) {
	        Optional<Notice> existingNotice = noticeRepository.findById(id);

	        if (existingNotice.isPresent()) {
	            Notice noticeToUpdate = existingNotice.get();

	           
	            if (updatedNotice.getNoticeDate() != null) {
	                noticeToUpdate.setNoticeDate(updatedNotice.getNoticeDate());
	            }
	            if (updatedNotice.getNotice() != null) {
	                noticeToUpdate.setNotice(updatedNotice.getNotice());
	            }
	            if (updatedNotice.getCreatedBy() != null) {
	                noticeToUpdate.setCreatedBy(updatedNotice.getCreatedBy());
	            }

	           
	            return noticeRepository.save(noticeToUpdate);
	        }

	        return null;
	    }
}
